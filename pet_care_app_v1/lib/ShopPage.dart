import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pet_care_app/Dashboard.dart';

class Shoppage extends StatefulWidget {
  const Shoppage({super.key});

  @override
  State createState() => _ShoppageState();
}

class _ShoppageState extends State {
  final TextEditingController _searchText = TextEditingController();

  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            decoration: const BoxDecoration(
                color: Color.fromRGBO(245, 146, 69, 1),
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(30),
                    bottomLeft: Radius.circular(30))),
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 20.0, right: 20, bottom: 0, top: 50),
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Hello Khushal,",
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400)),
                            const SizedBox(
                              height: 6,
                            ),
                            Text("Find your lovable Pets",
                                style: GoogleFonts.poppins(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white)),
                            const SizedBox(
                              height: 20,
                            )
                          ],
                        ),
                      ),
                      //Spacer(),
                      const Icon(
                        Icons.shopping_cart_outlined,
                        color: Colors.white,
                        size: 40,
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 20,
                      right: 20,
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(16),
                          border: Border.all(
                              color: const Color.fromRGBO(252, 219, 193, 1),
                              width: 2)),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0, right: 20),
                        child: TextField(
                          controller: _searchText,
                          decoration: const InputDecoration(
                              hintStyle: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 18,
                              ),
                              hintText: "search",
                              suffixIcon: Icon(
                                Icons.search_rounded,
                                color: Color.fromRGBO(252, 167, 69, 1),
                                size: 30,
                              ),
                              enabledBorder: InputBorder.none,
                              focusedBorder: InputBorder.none),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30.0, right: 30, top: 30),
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                        height: 180,
                        width: 160,
                        decoration: BoxDecoration(
                            color: const Color.fromRGBO(245, 146, 69, 1),
                            borderRadius: BorderRadius.circular(18),
                            image: const DecorationImage(
                                image: AssetImage("assets/shop1.png"))),
                        child: Container(
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(20),
                                bottomRight: Radius.circular(20)),
                            // color: Colors.white,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0, top: 8),
                            child: Text(
                              "Cloths ",
                              style: GoogleFonts.poppins(
                                  backgroundColor: Colors.white,
                                  color: const Color.fromRGBO(245, 146, 69, 1),
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        )),
                    const Spacer(),
                    Container(
                        height: 160,
                        width: 160,
                        decoration: BoxDecoration(
                            color: const Color.fromRGBO(245, 146, 69, 1),
                            borderRadius: BorderRadius.circular(18),
                            image: const DecorationImage(
                                image: AssetImage("assets/shop2.png"))),
                        child: Container(
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(20),
                                bottomRight: Radius.circular(20)),
                            // color: Colors.white,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0, top: 8),
                            child: Text(
                              "Food ",
                              style: GoogleFonts.poppins(
                                  backgroundColor: Colors.white,
                                  color: const Color.fromRGBO(245, 146, 69, 1),
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        )),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Container(
                        height: 160,
                        width: 160,
                        decoration: BoxDecoration(
                            color: const Color.fromRGBO(245, 146, 69, 1),
                            borderRadius: BorderRadius.circular(18),
                            image: const DecorationImage(
                                image: AssetImage("assets/shop2.png"))),
                        child: Container(
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(20),
                                bottomRight: Radius.circular(20)),
                            // color: Colors.white,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0, top: 8),
                            child: Text(
                              "Food ",
                              style: GoogleFonts.poppins(
                                  backgroundColor: Colors.white,
                                  color: const Color.fromRGBO(245, 146, 69, 1),
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        )),
                    const Spacer(),
                    Container(
                        height: 180,
                        width: 160,
                        decoration: BoxDecoration(
                            color: const Color.fromRGBO(245, 146, 69, 1),
                            borderRadius: BorderRadius.circular(18),
                            image: const DecorationImage(
                                image: AssetImage("assets/shop1.png"))),
                        child: Container(
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(20),
                                bottomRight: Radius.circular(20)),
                            // color: Colors.white,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0, top: 8),
                            child: Text(
                              "Cloths ",
                              style: GoogleFonts.poppins(
                                  backgroundColor: Colors.white,
                                  color: const Color.fromRGBO(245, 146, 69, 1),
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        )),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Container(
                        height: 180,
                        width: 160,
                        decoration: BoxDecoration(
                            color: const Color.fromRGBO(245, 146, 69, 1),
                            borderRadius: BorderRadius.circular(18),
                            image: const DecorationImage(
                                image: AssetImage("assets/shop1.png"))),
                        child: Container(
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(20),
                                bottomRight: Radius.circular(20)),
                            // color: Colors.white,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0, top: 8),
                            child: Text(
                              "Cloths ",
                              style: GoogleFonts.poppins(
                                  backgroundColor: Colors.white,
                                  color: const Color.fromRGBO(245, 146, 69, 1),
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        )),
                    const Spacer(),
                    Container(
                        height: 160,
                        width: 160,
                        decoration: BoxDecoration(
                            color: const Color.fromRGBO(245, 146, 69, 1),
                            borderRadius: BorderRadius.circular(18),
                            image: const DecorationImage(
                                image: AssetImage("assets/shop2.png"))),
                        child: Container(
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(20),
                                bottomRight: Radius.circular(20)),
                            // color: Colors.white,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0, top: 8),
                            child: Text(
                              "Food ",
                              style: GoogleFonts.poppins(
                                  backgroundColor: Colors.white,
                                  color: const Color.fromRGBO(245, 146, 69, 1),
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        )),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
      bottomNavigationBar: NavigationBar(
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        height: 80,
        onDestinationSelected: (int index) {
          setState(() {
            currentIndex = index;
          });
        },
        selectedIndex: currentIndex,
        destinations: [
          NavigationDestination(
              icon: IconButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => const Dashboard()));
                  },
                  icon: const Icon(Icons.home_rounded)),
              // icon: Icon(
              //   Icons.home_rounded,
              //   size: 30,
              //   color: Color.fromRGBO(245, 146, 69, 1),
              // ),
              selectedIcon: Icon(Icons.home_rounded),
              label: "Home"),
          NavigationDestination(
              icon: Icon(
                Icons.favorite_border_rounded,
                size: 30,
                color: Color.fromRGBO(245, 146, 69, 1),
              ),
              label: "Services"),
          NavigationDestination(
              icon: Icon(
                Icons.shopping_cart_rounded,
                size: 30,
                color: Color.fromRGBO(245, 146, 69, 1),
              ),
              label: "Shop"),
          NavigationDestination(
              icon: Icon(
                Icons.watch_later_outlined,
                size: 30,
                color: Color.fromRGBO(245, 146, 69, 1),
              ),
              label: "History"),
          NavigationDestination(
              icon: Icon(
                Icons.account_circle_outlined,
                size: 30,
                color: Color.fromRGBO(245, 146, 69, 1),
              ),
              label: "Profile"),
        ],
      ),
    );
  }
}


import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pet_care_app/Notification.dart';
import 'package:pet_care_app/ServicePage.dart';
import 'package:pet_care_app/GroomingPage.dart';
import 'package:pet_care_app/ShopPage.dart';
import 'package:pet_care_app/TrainingPage.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({super.key});

  @override
  State createState() => _DashboardState();
}

class _DashboardState extends State {
  final TextEditingController _searchText = TextEditingController();

  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(245, 245, 247, 1),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 40,
              ),
              Row(
                children: [
                  Image.asset(
                    "assets/profile.png",
                    scale: 0.8,
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Column(
                    children: [
                      Text("Hello, Khushal",
                          style: GoogleFonts.poppins(
                              fontSize: 18, fontWeight: FontWeight.bold)),
                      const SizedBox(
                        height: 6,
                      ),
                      Text("Good Morning!",
                          style: GoogleFonts.poppins(
                              color: const Color.fromARGB(255, 120, 120, 120),
                              fontSize: 18,
                              fontWeight: FontWeight.w400)),
                    ],
                  ),
                  const Spacer(),
                  IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Notifypage()));
                    },
                    icon: const Icon(
                      Icons.notifications_none_rounded,
                      size: 32,
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    border: Border.all(
                        color: const Color.fromRGBO(245, 146, 69, 1),
                        width: 2)),
                child: Padding(
                  padding: const EdgeInsets.only(left: 20.0, right: 20),
                  child: TextField(
                    controller: _searchText,
                    decoration: const InputDecoration(
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w400, fontSize: 18),
                        hintText: "search",
                        suffixIcon: Icon(
                          Icons.search_rounded,
                          color: Color.fromRGBO(245, 146, 69, 1),
                          size: 30,
                        ),
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(1, -1),
                          blurRadius: 10,
                          color: Colors.black12)
                    ]),
                child: Padding(
                  padding:
                      const EdgeInsets.only(left: 16.0, right: 0, bottom: 0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("In love with Pets?",
                                style: GoogleFonts.poppins(
                                    fontSize: 20, fontWeight: FontWeight.w600)),
                            const SizedBox(
                              height: 6,
                            ),
                            Text("Get all what you need for them",
                                style: GoogleFonts.poppins(
                                    color:
                                        const Color.fromRGBO(245, 146, 69, 1),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400)),
                          ],
                        ),
                      ),
                      //Spacer(),
                      Image.asset(
                        "assets/banner1.png",
                      )
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Text(
                    "Category",
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600, fontSize: 18),
                  ),
                  const Spacer(),
                  Text(
                    "See all",
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        color: Colors.grey),
                  )
                ],
              ),
              const SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>const Servicepage()));
                    },
                    child: Container(
                      child: Column(
                        children: [
                          Image.asset(
                            "assets/category1.png",
                            scale: 0.8,
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Text(
                            "Veterinary",
                            style: GoogleFonts.poppins(fontSize: 14),
                          )
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>const Groomingpage()));
                    },
                    child: Column(
                      children: [
                        Image.asset(
                          "assets/category2.png",
                          scale: 0.8,
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          "Grooming",
                          style: GoogleFonts.poppins(fontSize: 14),
                        )
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      Image.asset(
                        "assets/category1.png",
                        scale: 0.8,
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Text(
                        "Pet Store",
                        style: GoogleFonts.poppins(fontSize: 14),
                      )
                    ],
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>const Trainpage()));
                    },
                    child: Column(
                      children: [
                        Image.asset(
                          "assets/category2.png",
                          scale: 0.8,
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          "Training",
                          style: GoogleFonts.poppins(fontSize: 14),
                        )
                      ],
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                "Events",
                style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600, fontSize: 18),
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(1, -1),
                          blurRadius: 10,
                          color: Colors.black12)
                    ]),
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 16.0, right: 0, bottom: 16, top: 16),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                                "Find and Join in Special Events For Your Pets!",
                                style: GoogleFonts.poppins(
                                    fontSize: 18, fontWeight: FontWeight.w500)),
                            const SizedBox(
                              height: 12,
                            ),
                            Container(
                              height: 50,
                              width: 120,
                              decoration: BoxDecoration(
                                  color: const Color.fromRGBO(245, 146, 69, 1),
                                  borderRadius: BorderRadius.circular(12)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "See More",
                                    style: GoogleFonts.poppins(
                                        fontSize: 16, color: Colors.white),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      //Spacer(),
                      Image.asset(
                        "assets/event1.png",
                        scale: 0.9,
                      )
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                "Community",
                style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600, fontSize: 18),
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(1, -1),
                          blurRadius: 10,
                          color: Colors.black12)
                    ]),
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 16.0, right: 0, bottom: 16, top: 16),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Connect and share with communities!",
                                style: GoogleFonts.poppins(
                                    fontSize: 18, fontWeight: FontWeight.w500)),
                            const SizedBox(
                              height: 12,
                            ),
                            Container(
                              height: 50,
                              width: 120,
                              decoration: BoxDecoration(
                                  color: const Color.fromRGBO(245, 146, 69, 1),
                                  borderRadius: BorderRadius.circular(12)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "See More",
                                    style: GoogleFonts.poppins(
                                        fontSize: 16, color: Colors.white),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      //Spacer(),
                      Image.asset(
                        "assets/banner1.png",
                        scale: 0.9,
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: NavigationBar(
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        height: 80,
        onDestinationSelected: (int index) {
          setState(() {
            currentIndex = index;
          });
        },
        selectedIndex: currentIndex,
        destinations:  [
          NavigationDestination(
              icon: Icon(
                Icons.home_rounded,
                size: 30,
                color: Color.fromRGBO(245, 146, 69, 1),
              ),
              selectedIcon: Icon(Icons.home_rounded),
              label: "Home"),
          NavigationDestination(
              icon: Icon(
                Icons.favorite_border_rounded,
                size: 30,
                color: Color.fromRGBO(245, 146, 69, 1),
              ),
              label: "Services"),
          NavigationDestination(
              icon: IconButton(onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>Shoppage()));
              }, icon: Icon(
                Icons.shopping_cart_rounded,
                size: 30,
                color: Color.fromRGBO(245, 146, 69, 1),
              ),),
              label: "Shop"),
          NavigationDestination(
              icon: Icon(
                Icons.watch_later_outlined,
                size: 30,
                color: Color.fromRGBO(245, 146, 69, 1),
              ),
              label: "History"),
          NavigationDestination(
              icon: Icon(
                Icons.account_circle_outlined,
                size: 30,
                color: Color.fromRGBO(245, 146, 69, 1),
              ),
              label: "Profile"),
        ],
      ),
    );
  }
}

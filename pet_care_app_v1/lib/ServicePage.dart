import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pet_care_app/Dashboard.dart';
import 'package:pet_care_app/DoctorProfile.dart';

class Servicepage extends StatefulWidget {
  const Servicepage({super.key});

  @override
  State createState() => _ServicePageState();
}

class _ServicePageState extends State {
  final TextEditingController _searchText = TextEditingController();

  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(245, 245, 247, 1),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 40,
              ),
              Row(
                children: [
                  const Icon(
                    Icons.location_on_outlined,
                    color: Color.fromRGBO(245, 146, 69, 1),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Pune",
                    style: GoogleFonts.poppins(fontSize: 16),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                decoration: const BoxDecoration(
                    color: Color.fromRGBO(245, 146, 69, 1),
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(1, -1),
                          blurRadius: 10,
                          color: Colors.black12)
                    ]),
                child: Padding(
                  padding:
                      const EdgeInsets.only(left: 16.0, right: 0, bottom: 0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Let's Find Specialist",
                                style: GoogleFonts.poppins(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white)),
                            const SizedBox(
                              height: 6,
                            ),
                            Text("Doctor For Your Pet!",
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400)),
                          ],
                        ),
                      ),
                      //Spacer(),
                      Image.asset(
                        "assets/doctor.png",
                      )
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    border: Border.all(
                        color: const Color.fromRGBO(245, 146, 69, 1),
                        width: 2)),
                child: Padding(
                  padding: const EdgeInsets.only(left: 20.0, right: 20),
                  child: TextField(
                    controller: _searchText,
                    decoration: const InputDecoration(
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w400, fontSize: 18),
                        hintText: "search",
                        suffixIcon: Icon(
                          Icons.search_rounded,
                          color: Color.fromRGBO(245, 146, 69, 1),
                          size: 30,
                        ),
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Text(
                    "Our Services",
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600, fontSize: 18),
                  ),
                  const Spacer(),
                  Text(
                    "See all",
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        color: Colors.grey),
                  )
                ],
              ),
              const SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Servicepage()));
                    },
                    child: Container(
                      child: Column(
                        children: [
                          Image.asset(
                            "assets/category1.png",
                            scale: 0.8,
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Text(
                            "Veterinary",
                            style: GoogleFonts.poppins(
                              fontSize: 14,
                              color: const Color.fromRGBO(245, 146, 69, 1),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      Image.asset(
                        "assets/category2.png",
                        scale: 0.8,
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Text(
                        "Grooming",
                        style: GoogleFonts.poppins(
                          fontSize: 14,
                          color: const Color.fromRGBO(245, 146, 69, 1),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Image.asset(
                        "assets/category1.png",
                        scale: 0.8,
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Text(
                        "Pet Store",
                        style: GoogleFonts.poppins(
                          fontSize: 14,
                          color: const Color.fromRGBO(245, 146, 69, 1),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Image.asset(
                        "assets/category2.png",
                        scale: 0.8,
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Text(
                        "Training",
                        style: GoogleFonts.poppins(
                          fontSize: 14,
                          color: const Color.fromRGBO(245, 146, 69, 1),
                        ),
                      )
                    ],
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                "Best Specialist Nearby",
                style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600, fontSize: 18),
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(1, -1),
                          blurRadius: 10,
                          color: Colors.black12)
                    ]),
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const DoctorprofilePage()));
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 5.0, right: 5, bottom: 5, top: 5),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          "assets/doctor.png",
                          //scale: 0.9,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Dr. Anna Jhonson",
                                  style: GoogleFonts.poppins(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500)),
                              const SizedBox(
                                height: 8,
                              ),
                              Text("Veterinary Behavioral",
                                  style: GoogleFonts.poppins(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.grey.shade600)),
                              const SizedBox(
                                height: 12,
                              ),
                              Row(
                                children: [
                                  const Icon(
                                    Icons.star_border_rounded,
                                    color: Color.fromRGBO(245, 146, 69, 1),
                                  ),
                                  Text("4.99",
                                      style: GoogleFonts.poppins(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.grey.shade600)),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  const Icon(
                                    Icons.location_on_outlined,
                                    color: Color.fromRGBO(245, 146, 69, 1),
                                  ),
                                  Text("1.5km",
                                      style: GoogleFonts.poppins(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.grey.shade600)),
                                ],
                              )
                            ],
                          ),
                        ),
                        //Spacer(),
                      ],
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(1, -1),
                          blurRadius: 10,
                          color: Colors.black12)
                    ]),
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 5.0, right: 5, bottom: 5, top: 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/doctor.png",
                        //scale: 0.9,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Dr. De Armas",
                                style: GoogleFonts.poppins(
                                    fontSize: 18, fontWeight: FontWeight.w500)),
                            const SizedBox(
                              height: 8,
                            ),
                            Text("Veterinary Surgerical",
                                style: GoogleFonts.poppins(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.grey.shade600)),
                            const SizedBox(
                              height: 12,
                            ),
                            Row(
                              children: [
                                const Icon(
                                  Icons.star_border_rounded,
                                  color: Color.fromRGBO(245, 146, 69, 1),
                                ),
                                Text("4.0",
                                    style: GoogleFonts.poppins(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.grey.shade600)),
                                const SizedBox(
                                  width: 8,
                                ),
                                const Icon(
                                  Icons.location_on_outlined,
                                  color: Color.fromRGBO(245, 146, 69, 1),
                                ),
                                Text("2.0km",
                                    style: GoogleFonts.poppins(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.grey.shade600)),
                              ],
                            )
                          ],
                        ),
                      ),
                      //Spacer(),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: NavigationBar(
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        height: 80,
        onDestinationSelected: (int index) {
          setState(() {
            currentIndex = index;
          });
        },
        selectedIndex: currentIndex,
        destinations: [
          NavigationDestination(
              icon: IconButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Dashboard()));
                  },
                  icon: Icon(Icons.home_rounded)),
              // icon: Icon(
              //   Icons.home_rounded,
              //   size: 30,
              //   color: Color.fromRGBO(245, 146, 69, 1),
              // ),
              selectedIcon: Icon(Icons.home_rounded),
              label: "Home"),
          NavigationDestination(
              icon: Icon(
                Icons.favorite_border_rounded,
                size: 30,
                color: Color.fromRGBO(245, 146, 69, 1),
              ),
              label: "Services"),
          NavigationDestination(
              icon: Icon(
                Icons.shopping_cart_rounded,
                size: 30,
                color: Color.fromRGBO(245, 146, 69, 1),
              ),
              label: "Shop"),
          NavigationDestination(
              icon: Icon(
                Icons.watch_later_outlined,
                size: 30,
                color: Color.fromRGBO(245, 146, 69, 1),
              ),
              label: "History"),
          NavigationDestination(
              icon: Icon(
                Icons.account_circle_outlined,
                size: 30,
                color: Color.fromRGBO(245, 146, 69, 1),
              ),
              label: "Profile"),
        ],
      ),
    );
  }
}
